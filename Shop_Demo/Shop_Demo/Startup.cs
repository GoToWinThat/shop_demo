﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Hangfire;
using Hangfire.SqlServer;
using Hangfire.Dashboard;
using Shop_Demo.Infrastructure;

[assembly: OwinStartup(typeof(Shop_Demo.Startup))]

namespace Shop_Demo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            GlobalConfiguration.Configuration.UseSqlServerStorage("StoreContext");

            //        var options = new DashboardOptions
            //        {
            //            Authorization = new[]
            //{
            //        new AuthorizationFilter { Users = "admin, superuser", Roles = "advanced" },
            //        new ClaimsBasedAuthorizationFilter("name", "value")
            //    }
            //       };


            app.UseHangfireDashboard("/hangfire", new DashboardOptions
            {
                Authorization = new[] { new MyAuthorizationFilter { Users = "admin", Roles = "admin" } }
            });
            app.UseHangfireServer();
        }
    }
}