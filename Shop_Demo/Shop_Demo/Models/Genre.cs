﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shop_Demo.Models
{
    public class Genre
    {
        public int GenreId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string IconFilename { get; set; }

        //Allows to get all Albums with one type of genre
        public ICollection<Album> Albums { get; set; }
    }
}