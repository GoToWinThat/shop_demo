﻿using Hangfire.Dashboard;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shop_Demo.Infrastructure
{
    public class MyAuthorizationFilter : IDashboardAuthorizationFilter
    {
        //
        // Summary:
        //     Gets or sets the authorized roles.
        //
        // Remarks:
        //     Multiple role names can be specified using the comma character as a separator.
        public string Roles { get; set; }
        //
        // Summary:
        //     Gets or sets the authorized users.
        //
        // Remarks:
        //     Multiple role names can be specified using the comma character as a separator.
        public string Users { get; set; }

        public bool Authorize(DashboardContext context)
        {

            // In case you need an OWIN context, use the next line, `OwinContext` class
            // is the part of the `Microsoft.Owin` package.
            var owinContext = new OwinContext(context.GetOwinEnvironment());

            // Allow all authenticated users to see the Dashboard (potentially dangerous).
            return owinContext.Authentication.User.Identity.IsAuthenticated;
        }
    }
}