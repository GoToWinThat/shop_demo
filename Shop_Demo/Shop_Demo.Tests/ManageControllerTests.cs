﻿using System;
using System.Data.Entity;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Shop_Demo.Controllers;
using Shop_Demo.DataAccessLayer;
using Shop_Demo.Infrastructure;
using Shop_Demo.Models;

namespace Shop_Demo.Tests
{
    [TestClass]
    public class ManageControllerTests
    {
        // Mocking our own IMailService
        [TestMethod]
        public void ChangeOrderState_OrderstateChangesToShipped_SendsConfirmationEmail()
        {
            // Arrange            
            var mockSet = new Mock<DbSet<Order>>();
            Order orderToModify = new Order { OrderId = 1, OrderState = OrderState.New };
            mockSet.Setup(m => m.Find(It.IsAny<int>())).Returns(orderToModify);

            var mockContext = new Mock<StoreContext>();
            mockContext.Setup(c => c.Orders).Returns(mockSet.Object);

            // Mock MailService
            var mailMock = new Mock<IMailService>();

            Order orderArgument = new Order { OrderId = 1, OrderState = OrderState.Shipped };

            var controller = new ManageController(mockContext.Object, mailMock.Object);

            // Act
            var result = controller.ChangeOrderState(orderArgument);

            // Assert
            mailMock.Verify(m => m.SendOrderShippedEmail(It.IsAny<Order>()), Times.Once());
        }
    }
}
